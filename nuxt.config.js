/* global module */
module.exports = {
  /*
  ** For dev only- delete after production
  */
  // cache: false,
  /*
  ** Headers of the page
  */
  head: {
    title: 'Bihaber',
    htmlAttrs: {
      lang: 'tr'
    },
    bodyAttrs: {
      ref: 'thebody'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0' },
      { hid: 'description', name: 'description', content: 'Bihaber mobile' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '~assets/css/main.css',
    { src: '~assets/scss/app.scss', lang: 'scss' } // scss instead of sass
  ],
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#cc0000' },
  /*
  ** Build configuration
  */
  alias: {
    // 'hammerjs$': 'vue-touch/dist/hammer-ssr.js'
  },
  performance: {
    hints: process.env.NODE_ENV === 'production' ? 'warning' : false
  },
  plugins: [
    // { src: '~plugins/vue-touch.js', ssr: false },
    // { src: '~plugins/vue-lory.js', ssr: false }
  ],
  build: {
    /*
    ** Run ESLINT on save
    */
    // vendor: ['axios', 'vue-touch', 'vue-lory', 'lory.js'],
    vendor: ['axios'],
    extend (config, { isClient }) {
      if (isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
