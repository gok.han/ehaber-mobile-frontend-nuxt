export const state = {
  messages: ['console ready...'] // will be used to import list data
}

export const actions = {
  ADD_LOG: function ({ commit }, message) {
    commit('APPEND_LOG', message)
  }
}

export const mutations = {
  APPEND_LOG (state, message) {
    // (!state.messages === true) ? (state.messages = message) : (state.messages = state.messages.concat(message))
    // (!state.messages === true) ? ('') : (state.messages = message.toString())
    (!state.messages === true) ? ('') : (state.messages = state.messages.concat(message))
  }
}
