import axios from 'axios'
import apiRoutes from '~components/apiRoutes.js'

const apiEndPoint = apiRoutes.detail_article

/*
function timeOut (min, max) {
  // console.log('mimic latency') // FIXME remove me in the production and remove
  return Math.floor(Math.random() * (max - min + 1)) + min
}
*/
export const state = {
  list: {}, // will be used to import list data
  api_headers: {} // will get the headers and be populated everytime
}

export const actions = {
  LOAD_DETAIL: function ({ commit }, slug) {
    return axios.get(apiEndPoint + slug)
    .then((res) => {
      console.log('actions LOAD_DETAIL axios then init')
      // console.log(slug, res.data[0], res.headers)
      // setTimeout(() => {
        // In order to load the headers, we create an object with headers.
      commit('CONCAT_DETAIL_DATA', [slug, res.data[0], res.headers])
      // }, timeOut(200, 500))
    }, (err) => {
      console.log('store LOAD_DETAIL action', err)
    })
  }
}

export const mutations = {
  CONCAT_DETAIL_DATA (state, incomingDataArray) { // In order to load the headers, we get an object with headers.
    // console.log('MUT', incomingDataArray[0], incomingDataArray[1], incomingDataArray[2])
    // console.log('incomingData', incomingDataArray)
    console.log('mutations CONCAT_DETAIL_DATA init')
    state.api_headers = incomingDataArray[2]
    // for list type Object
    !state.list === true ? (state.list = {[incomingDataArray[0]]: incomingDataArray[1]}) : (state.list = Object.assign(state.list, {[incomingDataArray[0].toString()]: incomingDataArray[1]}))
    // for list type incomingDataArray
    // !state.list === true ? (state.list = {[incomingDataArray[0]]: incomingDataArray[1]}) : (state.list = state.list.concat({[incomingDataArray[0]]: incomingDataArray[1]}))
  }
}
