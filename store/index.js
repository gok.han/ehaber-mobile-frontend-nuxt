export const state = {
  pageTitle: 'bihaber',
  appLayout: {
    isSidebarOpen: false,
    isSearchFormOpen: false,
    isFilterFormOpen: false
  },
  topbarMenu: {
    isBackIconVisible: false,
    isCardToggleIconVisible: false,
    isFilterIconVisible: false,
    isSearchIconVisible: false,
    isSettingsIconVisible: false,
    isAddFavoritesIconVisible: false
  }
}

export const actions = {
  SET_PAGE_TITLE: function ({ commit }, newTitle) {
    commit('setPageTitle', newTitle.toString())
  }
}

export const mutations = {
  increment (state) {
    state.counter++
  },
  toggeSidebar (state) {
    state.appLayout.isSidebarOpen = !state.appLayout.isSidebarOpen
  }, // form components are not written yet
  closeSidebar (state) {
    if (state.appLayout.isSidebarOpen) {
      state.appLayout.isSidebarOpen = false
    }
  },
  toggeFilterForm (state) {
    state.appLayout.isFilterFormOpen = !state.appLayout.isFilterFormOpen
    alert('filter form opened')
  },
  toggeSearchForm (state) {
    state.appLayout.isFilterFormOpen = !state.appLayout.isFilterFormOpen
    alert('search form opened')
  },
  toggleBackButton (state) {
    state.topbarMenu.isBackIconVisible = !state.topbarMenu.isBackIconVisible
  },
  pushTopBarLayout (state, newData) {
    state.topbarMenu = newData
  },
  setPageTitle (state, newTitle) {
    state.pageTitle = newTitle
  }
}
