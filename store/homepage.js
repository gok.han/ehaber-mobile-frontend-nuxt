// eslint-disable-next-line
import axios from 'axios'
import apiRoutes from '~components/apiRoutes.js'

const apiEndPoint = apiRoutes.homepage
const listLength = apiRoutes.list_lenght

// function timeOut (min, max) {
//   // console.log('mimic latency') // FIXME remove me in the production and remove
//   return Math.floor(Math.random() * (max - min + 1)) + min
// }
export const state = {
  headlines: {}, // will be used to import header objects data
  list: {}, // will be used to import list data
  api_headers: {} // will get the headers and be populated everytime
}
/* api_headers {}
    cache-control:"no-cache"
    content-type:"application/json; charset=utf-8"
    expires:"-1"
    link:"<http://localhost:2222/son-dakika/sayfa/1>; rel=\"first\", <http://localhost:2222/son-dakika/sayfa/1>; rel=\"next\", <http://localhost:2222/son-dakika/sayfa/1>; rel=\"last\""
    pragma:"no-cache"
    x-total-count:"100"
*/
export const actions = {
  LOADMORE_LISTVIEW_DATA: function ({ commit }, viewCount = 1) {
    return axios.get(apiEndPoint + Math.floor((viewCount / listLength) + 1))
    .then((res) => {
      // setTimeout(() => {
      commit('SET_API_HEADERS_DATA', res.headers)
      commit('SET_HEADLINES_DATA', res.data.headlines)
      commit('CONCAT_LISTVIEW_DATA', res.data.news_area)
      // }, timeOut(200, 500))
    }, (err) => {
      console.log(err)
    })
  }
}

export const mutations = {
  SET_API_HEADERS_DATA (state, incomingData) {
    state.api_headers = Object.assign(incomingData, {'x-total-count': '100'})
  },
  SET_HEADLINES_DATA (state, incomingData) {
    state.headlines = Object.assign(state.headlines, incomingData)
  },
  CONCAT_LISTVIEW_DATA (state, incomingData) {
    state.list = Object.assign(state.list, incomingData)
  }
}
