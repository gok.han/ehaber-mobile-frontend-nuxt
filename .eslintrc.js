module.exports = {
  root: true,
  parser: 'babel-eslint',
  env: {
    browser: true,
    node: true,
    es6: true
  },
  extends: 'standard',
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  "properties": "warn",
  // add your custom rules here
  "rules": {
    "func-names":"off",
    "indent": [
      "warn",
      2
    ]
    // "linebreak-style": [
    //     "warn",
    //     "unix"
    // ],
    // "quotes": [
    //     "warn",
    //     "single"
    // ],
    // "semi": [
    //     "warn",
    //     "always"
    // ]
  },
  globals: {}
};