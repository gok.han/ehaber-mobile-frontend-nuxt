// this file manages the whole api endpoints
const apiRootRemote = 'https://skati.herokuapp.com/' // documentation homepage
const apiRootLocal = 'http://localhost:2222/'
// const apiRootLAN = 'http://haberapi.app:8000/mobileweb/'
// const apiRootLocal = 'http://haberapi.app:8000/mobileweb/'

let apiRoot = (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') ? apiRootLocal : apiRootRemote
// console.log('process.env NODE_ENV?', process.env.NODE_ENV)
// const envSetting = process.env.IS_OVER_LAN
// console.log('process.env IS_OVER_LAN?', envSetting)

// switch (process.env.IS_OVER_LAN) {
// case 'development' : {
//   apiRoot = apiRootLocal
//   break
// }
// case 'production' : {
//   apiRoot = apiRootRemote
//   break
// }
// case 'over_lan': {
//   apiRoot = apiRootLocal
//   break
// }
// default: {
//   apiRoot = apiRootLAN
//   break
// }
// }

console.log('apiRoot', apiRoot)

export default {
  list_lenght: 10, // if the api changes change here also
  homepage: apiRoot + 'homepage', // :page{Number} (optional) limit by 10, page 1, 10 is set from API
  son_dk_list: apiRoot + 'son-dakika/', // :page{Number} (optional) limit by 10, page 1, 10 is set from API
  detail_article: apiRoot + 'detaylar/' // :slug{String} get page
}
